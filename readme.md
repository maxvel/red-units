# Red units convertor library

This is a library to convert between different units.
It is base on `units` Unix utility, the data file is created
by parsing `/usr/share/units/definitions.units`

## Features

* Currently supports 960 units, 1124 constants and 107 prefixes

## Examples
    >> do %units.red
    == make object! [
        graph: #()
        prefixes: make hash! []
        constants: make hash! []
        a...
    >> units/load load %units.data
    == [
        units: 958 constants: 1124 prefixes: 107
    ]
    >> units/convert 5 'moonmass 'earthmass
    == 0.06150017
    >> units/convert 1 'Mkg 'tonne
    == 1000.0
    >> units/convert 1 'megakg 'tonne
    == 1000.0
    >> units/convert 100 'blink 'hour
    == 0.024
    >> units/convert 100 'blink 'minute
    == 1.44
    >> units/convert 5 'minute 'blink
    == 347.2222222222222
    >> units/convert 'million 'blink 'day
    == 10.0

## Misc

### List of basic units

    kg
    s
    m
    A
    cd
    mol
    K
    radian
    sr
    bit
    wholenote

### List of prefixes (in no order)

    yotta 
    zetta 
    exa 
    peta 
    tera 
    giga 
    mega 
    myria 
    kilo 
    hecto 
    deca 
    deka 
    deci 
    centi 
    milli 
    micro 
    nano 
    pico 
    femto 
    atto 
    zepto 
    yocto 
    quarter 
    semi 
    demi 
    hemi 
    half 
    double 
    triple 
    treble 
    kibi 
    mebi 
    gibi 
    tebi 
    pebi 
    exbi 
    Ki 
    Mi 
    Gi 
    Ti 
    Pi 
    Ei 
    Y 
    Z 
    E 
    P 
    T 
    G 
    M 
    k 
    h 
    da 
    d 
    c 
    m 
    u 
    n 
    p 
    f 
    a 
    z 
    y 
    US 
    survey 
    geodetic 
    int 
    Zena 
    Duna 
    Trina 
    Quedra 
    Quena 
    Hesa 
    Seva 
    Aka 
    Neena 
    Dexa 
    Lefa 
    Zennila 
    Zeni 
    Duni 
    Trini 
    Quedri 
    Queni 
    Hesi 
    Sevi 
    Aki 
    Neeni 
    Dexi 
    Lefi 
    Zennili 
    ⅛ 
    ¼ 
    ⅜ 
    ½ 
    ⅝ 
    ¾ 
    ⅞ 
    ⅙ 
    ⅓ 
    ⅔ 
    ⅚ 
    ⅕ 
    ⅖ 
    ⅗ 
    ⅘ 
    µ 
    μ

### List of constants (in no order)

    pi 3.141592653589793 
    yotta 1.0e24 
    zetta 1.0e21 
    exa 1.0e18 
    peta 1000000000000000.0 
    tera 1000000000000.0 
    giga 1000000000.0 
    mega 1000000.0 
    myria 10000.0 
    kilo 1000.0 
    hecto 100.0 
    deca 10.0 
    deka 10.0 
    deci 0.1 
    centi 0.01 
    milli 0.001 
    micro 1.0e-6 
    nano 1.0e-9 
    pico 1.0e-12 
    femto 1.0e-15 
    atto 0.0 
    zepto 0.0 
    yocto 0.0 
    quarter 0.25 
    semi 0.5 
    demi 0.5 
    hemi 0.5 
    half 0.5 
    double 2.0 
    triple 3.0 
    treble 3.0 
    kibi 1024.0 
    mebi 1048576.0 
    gibi 1073741824.0 
    tebi 1099511627776.0 
    pebi 1125899906842624.0 
    exbi 1.152921504606847e18 
    Ki 1024.0 
    Mi 1048576.0 
    Gi 1073741824.0 
    Ti 1099511627776.0 
    Pi 1125899906842624.0 
    Ei 1.152921504606847e18 
    Y 1.0e24 
    Z 1.0e21 
    E 1.0e18 
    P 1000000000000000.0 
    T 1000000000000.0 
    G 1000000000.0 
    M 1000000.0 
    k 1000.0 
    h 100.0 
    da 10.0 
    d 0.1 
    c 0.01 
    m 0.001 
    u 1.0e-6 
    n 1.0e-9 
    p 1.0e-12 
    f 1.0e-15 
    a 0.0 
    z 0.0 
    y 0.0 
    one 1.0 
    two 2.0 
    double 2.0 
    couple 2.0 
    three 3.0 
    triple 3.0 
    four 4.0 
    quadruple 4.0 
    five 5.0 
    quintuple 5.0 
    six 6.0 
    seven 7.0 
    eight 8.0 
    nine 9.0 
    ten 10.0 
    eleven 11.0 
    twelve 12.0 
    thirteen 13.0 
    fourteen 14.0 
    fifteen 15.0 
    sixteen 16.0 
    seventeen 17.0 
    eighteen 18.0 
    nineteen 19.0 
    twenty 20.0 
    thirty 30.0 
    forty 40.0 
    fifty 50.0 
    sixty 60.0 
    seventy 70.0 
    eighty 80.0 
    ninety 90.0 
    hundred 100.0 
    thousand 1000.0 
    million 1000000.0 
    shortbillion 1000000000.0 
    shorttrillion 1000000000000.0 
    shortquadrillion 1000000000000000.0 
    shortquintillion 1.0e18 
    shortsextillion 1.0e21 
    shortseptillion 1.0e24 
    shortoctillion 1.0e27 
    shortnonillion 1.0e30 
    shortnoventillion 1.0e30 
    shortdecillion 1.0e33 
    shortundecillion 1.0e36 
    shortduodecillion 1.0e39 
    shorttredecillion 1.0e42 
    shortquattuordecillion 1.0e45 
    shortquindecillion 1.0e48 
    shortsexdecillion 1.0e51 
    shortseptendecillion 1.0e54 
    shortoctodecillion 1.0e57 
    shortnovemdecillion 1.0e60 
    shortvigintillion 1.0e63 
    centillion 1.0e303 
    googol 1.0e100 
    longbillion 1000000000000.0 
    longtrillion 1.0e18 
    longquadrillion 1.0e24 
    longquintillion 1.0e30 
    longsextillion 1.0e36 
    longseptillion 1.0e42 
    longoctillion 1.0e48 
    longnonillion 1.0e54 
    longnoventillion 1.0e54 
    longdecillion 1.0e60 
    longundecillion 1.0e66 
    longduodecillion 1.0e72 
    longtredecillion 1.0e78 
    longquattuordecillion 1.0e84 
    longquindecillion 1.0e90 
    longsexdecillion 1.0e96 
    longseptdecillion 1.0e102 
    longoctodecillion 1.0e108 
    longnovemdecillion 1.0e114 
    longvigintillion 1.0e120 
    milliard 1000000000.0 
    billiard 1.0e18 
    trilliard 1.0e27 
    quadrilliard 1.0e36 
    quintilliard 1.0e45 
    sextilliard 1.0e54 
    septilliard 1.0e63 
    octilliard 1.0e72 
    nonilliard 1.0e81 
    noventilliard 1.0e81 
    decilliard 1.0e90 
    longmilliard 1000000000.0 
    longbilliard 1.0e18 
    longtrilliard 1.0e27 
    longquadrilliard 1.0e36 
    longquintilliard 1.0e45 
    longsextilliard 1.0e54 
    longseptilliard 1.0e63 
    longoctilliard 1.0e72 
    longnonilliard 1.0e81 
    longnoventilliard 1.0e81 
    longdecilliard 1.0e90 
    billion 1000000000.0 
    trillion 1000000000000.0 
    quadrillion 1000000000000000.0 
    quintillion 1.0e18 
    sextillion 1.0e21 
    septillion 1.0e24 
    octillion 1.0e27 
    nonillion 1.0e30 
    noventillion 1.0e30 
    decillion 1.0e33 
    undecillion 1.0e36 
    duodecillion 1.0e39 
    tredecillion 1.0e42 
    quattuordecillion 1.0e45 
    quindecillion 1.0e48 
    sexdecillion 1.0e51 
    septendecillion 1.0e54 
    octodecillion 1.0e57 
    novemdecillion 1.0e60 
    vigintillion 1.0e63 
    lakh 100000.0 
    crore 10000000.0 
    arab 1000000000.0 
    kharab 100000000000.0 
    neel 10000000000000.0 
    padm 1000000000000000.0 
    shankh 1.0e17 
    are 0.01 
    a 0.01 
    angstrom 1.0e-13 
    xunit_cu 0.0 
    xunit_mo 0.0 
    angstromstar 1.00001495e-13 
    fermi 0.0 
    barn 0.0 
    shed 0.0 
    spat 1000000000.0 
    percent 0.01 
    % 0.01 
    mill 0.001 
    proof 0.005 
    ppm 1.0e-6 
    partspermillion 1.0e-6 
    ppb 1.0e-9 
    partsperbillion 1.0e-9 
    ppt 1.0e-12 
    partspertrillion 1.0e-12 
    karat 0.04166666666666666 
    caratgold 0.04166666666666666 
    basispoint 0.01 
    fine 0.001 
    gasmark 1.323975275623656e44 
    pi 3.141592653589793 
    light 0.01 
    energy 0.0001 
    hbar 157.0796326794897 
    spin 157.0796326794897 
    dalton 1.0e-6 
    stefanboltzmann 2.349663384501983e94 
    sigma 2.349663384501983e94 
    Rinfinity 10973731568.539 
    R_H 10967760000.0 
    alpha 0.0072973525664 
    bohrradius 62894047.3580087 
    deuteronchargeradius 0.0 
    protonchargeradius 0.0 
    electronmass 5.4857990907e-10 
    m_e 5.4857990907e-10 
    protonmass 1.007276466879e-6 
    m_p 1.007276466879e-6 
    neutronmass 1.00866491588e-6 
    m_n 1.00866491588e-6 
    muonmass 1.134289257e-7 
    m_mu 1.134289257e-7 
    deuteronmass 2.013553212745e-6 
    m_d 2.013553212745e-6 
    alphaparticlemass 4.001506179127e-6 
    m_alpha 4.001506179127e-6 
    taumass 1.90749e-6 
    m_tau 1.90749e-6 
    tritonmass 3.01550071632e-6 
    m_t 3.01550071632e-6 
    helionmass 3.01493224673e-6 
    m_h 3.01493224673e-6 
    protonwavelength 1.0e-9 
    lambda_C 1.0e-9 
    neutronwavelength 1.0e-6 
    lambda_C 1.0e-6 
    rydberg 10973731568.539 
    earthflattening 0.003352819697896193 
    earthradius_equatorial 6378.13649 
    sunradius 696000.0 
    gauss_k 0.01720209895 
    astronomicalunit 149597870.7 
    au 149597870.7 
    sundist 149598022.9607128 
    moondist 384400.0 
    sundist_near 147100000.0 
    sundist_far 152100000.0 
    moonearthmassratio 0.012300034 
    atomicmass 5.4857990907e-10 
    atomicaction 157.0796326794897 
    atomiclength 62894047.3580087 
    US 304800609.6012192 
    survey 304800609.6012192 
    geodetic 304800609.6012192 
    int 3.280833333333333e-9 
    nauticalmile 1.852 
    cable 0.1852 
    intcable 0.1852 
    cablelength 0.1852 
    marineleague 5.556 
    buck 304800609.6012192 
    fin 1524003048.006096 
    sawbuck 3048006096.012192 
    usgrand 304800609601.2192 
    greenback 304800609.6012192 
    sugar_conc_bpe 0.0 
    baumeconst 145.0 
    brix 2.609329312723311e156 
    pair 2.0 
    brace 2.0 
    nest 3.0 
    hattrick 3.0 
    dicker 10.0 
    dozen 12.0 
    bakersdozen 13.0 
    score 20.0 
    flock 40.0 
    timer 40.0 
    shock 60.0 
    toncount 100.0 
    longhundred 120.0 
    gross 144.0 
    greatgross 1728.0 
    tithe 0.1 
    shortquire 24.0 
    quire 25.0 
    shortream 480.0 
    ream 500.0 
    perfectream 516.0 
    bertholdpoint 3.759398496240602e-7 
    octave 2.0 
    majorthird 1.25 
    minorthird 1.2 
    musicalfourth 1.333333333333333 
    musicalfifth 1.5 
    majorsixth 1.666666666666667 
    minorsixth 1.6 
    majorseventh 1.875 
    minorseventh 1.8 
    pythagoreancomma 4835660372320.35 
    semitone 1.059463094359295 
    dotted 1.5 
    doubledotted 1.75 
    unitedstatesdollar 304800609.6012192 
    usdollar 304800609.6012192 
    stere 1.0e-9 
    air_1976 1.142324560639446e-5 
    earthradUSAtm 0.001 
    g00 -1.0 
    g000 -2.0 
    g0000 -3.0 
    g00000 -4.0 
    g000000 -5.0 
    g0000000 -6.0 
    brwiregauge 0.0001400045593061228 
    plategauge -5.923895217000284e25 
    stdgauge -5.923895217000284e25 
    zincgauge 0.71360352 
    grit_P 2.611225545081385e118 
    grit_F 1.883080231152574e159 
    ansibonded 6.638898175431796e148 
    grit_ansibonded 6.638898175431796e148 
    jisgrit 2.975622216191797e77 
    grit_A 1.363553784674119e44 
    nmi 1.852 
    nmile 1.852 
    actinium 227.0278 
    aluminum 26.981539 
    americium 243.0614 
    antimony 121.76 
    argon 39.948 
    arsenic 74.92159 
    astatine 209.9871 
    barium 137.327 
    berkelium 247.0703 
    beryllium 9.012182 
    bismuth 208.98037 
    boron 10.811 
    bromine 79.904 
    cadmium 112.411 
    calcium 40.078 
    californium 251.0796 
    carbon 12.011 
    cerium 140.115 
    cesium 132.90543 
    chlorine 35.4527 
    chromium 51.9961 
    cobalt 58.9332 
    copper 63.546 
    curium 247.0703 
    deuterium 2.0141017778 
    dysprosium 162.5 
    einsteinium 252.083 
    erbium 167.26 
    europium 151.965 
    fermium 257.0951 
    fluorine 18.9984032 
    francium 223.0197 
    gadolinium 157.25 
    gallium 69.723 
    germanium 72.61 
    gold 196.96654 
    hafnium 178.49 
    helium 4.002602 
    holmium 164.93032 
    hydrogen 1.00794 
    indium 114.818 
    iodine 126.90447 
    iridium 192.217 
    iron 55.845 
    krypton 83.8 
    lanthanum 138.9055 
    lawrencium 262.11 
    lead 207.2 
    lithium 6.941 
    lutetium 174.967 
    magnesium 24.305 
    manganese 54.93805 
    mendelevium 258.1 
    mercury 200.59 
    molybdenum 95.94 
    neodymium 144.24 
    neon 20.1797 
    neptunium 237.0482 
    nickel 58.6934 
    niobium 92.90638 
    nitrogen 14.00674 
    nobelium 259.1009 
    osmium 190.23 
    oxygen 15.9994 
    palladium 106.42 
    phosphorus 30.973762 
    platinum 195.08 
    plutonium 244.0642 
    polonium 208.9824 
    potassium 39.0983 
    praseodymium 140.90765 
    promethium 144.9127 
    protactinium 231.03588 
    radium 226.0254 
    radon 222.0176 
    rhenium 186.207 
    rhodium 102.9055 
    rubidium 85.4678 
    ruthenium 101.07 
    samarium 150.36 
    scandium 44.95591 
    selenium 78.96 
    silicon 28.0855 
    silver 107.8682 
    sodium 22.989768 
    strontium 87.62 
    sulfur 32.066 
    tantalum 180.9479 
    technetium 97.9072 
    tellurium 127.6 
    terbium 158.92534 
    thallium 204.3833 
    thorium 232.0381 
    thullium 168.93421 
    tin 118.71 
    titanium 47.867 
    tungsten 183.84 
    uranium 238.0289 
    vanadium 50.9415 
    xenon 131.29 
    ytterbium 173.04 
    yttrium 88.90585 
    zinc 65.39 
    zirconium 91.224 
    people 1.0 
    person 1.0 
    death 1.0 
    capita 1.0 
    Zena 12.0 
    Duna 144.0 
    Trina 1728.0 
    Quedra 20736.0 
    Quena 248832.0 
    Hesa 2985984.0 
    Seva 35831808.0 
    Aka 429981696.0 
    Neena 5159780352.0 
    Dexa 61917364224.0 
    Lefa 743008370688.0 
    Zennila 8916100448256.0 
    Zeni 0.08333333333333333 
    Duni 0.006944444444444444 
    Trini 0.0005787037037037037 
    Quedri 4.822530864197531e-5 
    Queni 4.018775720164609e-6 
    Hesi 3.348979766803841e-7 
    Sevi 2.790816472336534e-8 
    Aki 2.325680393613778e-9 
    Neeni 1.938066994678149e-10 
    Dexi 1.615055828898457e-11 
    Lefi 1.345879857415381e-12 
    Zennili 1.121566547846151e-13 
    wari_proportion 0.1 
    bu_proportion 0.01 
    rin_proportion 0.001 
    mou_proportion 0.0001 
    shaku 0.000303030303030303 
    mou 3.03030303030303e-8 
    rin 3.03030303030303e-7 
    bu_distance 3.03030303030303e-6 
    sun 3.030303030303031e-5 
    jou_distance 0.00303030303030303 
    kanejakusun 3.030303030303031e-5 
    kanejaku 0.000303030303030303 
    taichi 0.000303030303030303 
    taicun 3.030303030303031e-5 
    台尺 0.000303030303030303 
    台寸 3.030303030303031e-5 
    kujirajaku 0.0003787878787878788 
    kujirajakusun 3.787878787878788e-5 
    kujirajakubu 3.787878787878788e-6 
    kujirajakujou 0.003787878787878788 
    tan_distance 0.01136363636363636 
    ken 0.001818181818181818 
    chou_distance 0.1090909090909091 
    tsubo 0.00011900826446281 
    se 0.003570247933884298 
    tan_area 0.03570247933884298 
    ping 0.00011900826446281 
    jia 0.3491702479338843 
    fen 0.03491702479338844 
    fen_area 0.03491702479338844 
    坪 0.00011900826446281 
    甲 0.3491702479338843 
    分 0.03491702479338844 
    edoma 2.597910009182736e-5 
    kyouma 3.616382231404958e-5 
    chuukyouma 2.975206611570248e-5 
    jou_area 2.597910009182736e-5 
    shou 7.580990510540204e-9 
    to 7.580990510540204e-8 
    koku 7.580990510540204e-7 
    frenchfoot 0.0003248393849707645 
    pied 0.0003248393849707645 
    frenchfeet 0.0003248393849707645 
    frenchinch 2.70699487475637e-5 
    frenchthumb 2.70699487475637e-5 
    pouce 2.70699487475637e-5 
    frenchline 2.255829062296975e-6 
    ligne 2.255829062296975e-6 
    frenchpoint 1.879857551914146e-7 
    toise 0.001949036309824587 
    arpent 110.771332379347 
    dollar 304800609.6012192 
    cent 0.01 
    penny 0.01 
    grand 304800609601.2192 
    dollar 304800609.6012192 
    cent 0.01 
    ⅛ 0.125 
    ¼ 0.25 
    ⅜ 0.375 
    ½ 0.5 
    ⅝ 0.625 
    ¾ 0.75 
    ⅞ 0.875 
    ⅙ 0.1666666666666667 
    ⅓ 0.3333333333333333 
    ⅔ 0.6666666666666666 
    ⅚ 0.8333333333333334 
    ⅕ 0.2 
    ⅖ 0.4 
    ⅗ 0.6 
    ⅘ 0.8 
    µ 1.0e-6 
    μ 1.0e-6 
    ångström 1.0e-13 
    Å 1.0e-13 
    Å 1.0e-13 
    ¢ 0.01 
    ℎ 100.0 
    ℏ 157.0796326794897 
    ‰ 0.001 
    ‱ 0.0001 
    ㍲ 10.0 
    ㍳ 149597870.7 
    ㎛ 0.001 
    ㎡ 1.0e-6 
    ㎥ 1.0e-9 
    ㏀ 1000.0 
    ㏁ 1000000.0 
    ㏙ 1.0e-6

### List of all units

    A 
    aA 
    abamp 
    abampere 
    acetabula 
    acnua 
    acre 
    actus 
    actuslength 
    admiraltycable 
    admiraltymile 
    aeginadrachmae 
    aeginaobol 
    aeginastater 
    agate 
    akaina 
    amber 
    AMOUNT 
    amp 
    ampere 
    amphora 
    amphorae 
    amu 
    amu_chem 
    amu_phys 
    ANGLE 
    anomalisticmonth 
    apdram 
    apostilb 
    apounce 
    appound 
    apscruple 
    arabicfeet 
    arabicfoot 
    arabicinch 
    arabicmile 
    arabicsilverpound 
    arabictradepound 
    arcdeg 
    arcmin 
    arcminute 
    arcsec 
    arcsecond 
    AREA 
    arshin 
    asb 
    assyriancubit 
    assyrianfoot 
    assyrianpalm 
    assyriansusi 
    atomicmassunit 
    atticdrachmae 
    atticobol 
    atticstater 
    austbl 
    austblsp 
    austbsp 
    australiapoint 
    australiasquare 
    australiatablespoon 
    australiateaspoon 
    austsp 
    B 
    bag 
    balmer 
    balthazar 
    barleybushel 
    barrel 
    basebox 
    bbl 
    beat 
    beerkeg 
    beespace 
    bes 
    Bi 
    biblicalcubit 
    bicron 
    biot 
    bit 
    blackcubit 
    blanc 
    blink 
    blondel 
    bloodunit 
    boll 
    bolt 
    borderverst 
    borgis 
    bourgeois 
    brbarrel 
    brbeerbutt 
    brbeerhogshead 
    brbushel 
    brcable 
    brchaldron 
    brdessertspoon 
    breve 
    brevier 
    brfirkin 
    brgallon 
    brheapedbushel 
    brhogshead 
    brhundredweight 
    brillant 
    brilliant 
    brnauticalmile 
    bronzeyard11 
    brquarter 
    brshippington 
    brtablespoon 
    brtbl 
    brtblsp 
    brtbsp 
    brteaspoon 
    brton 
    brtsp 
    brwinehogshead 
    bucket 
    bushel 
    buttonline 
    byte 
    c 
    calendaryear 
    caliber 
    canada_oatbushel 
    candela 
    candle 
    canon 
    carat 
    catty 
    cc 
    ccf 
    cd 
    ce 
    cental 
    centner 
    centrad 
    centuria 
    centurium 
    ch 
    chain 
    chaldron 
    charriere 
    cheonix 
    chittak 
    choinix 
    cicero 
    circle 
    circleinch 
    circularinch 
    circularmil 
    click 
    clove 
    cm 
    cmil 
    cminv 
    coarsecrystalon 
    coarseindia 
    cochlearia 
    columbian 
    commonyear 
    computerpica 
    computerpoint 
    congii 
    congius 
    coomb 
    cord 
    cordfeet 
    cordfoot 
    cornbushel 
    corpus 
    cottonbolt 
    cran 
    cranberrybarrel 
    crith 
    crotchet 
    ct 
    culleus 
    cumec 
    CURRENT 
    cyathi 
    d 
    da 
    day 
    decimalhour 
    decimalminute 
    decimalsecond 
    deg 
    degC 
    degcelsius 
    degF 
    degfahrenheit 
    degK 
    degR 
    degrankine 
    degreaumur 
    degree 
    degreerankine 
    degreesrankine 
    demisemiquaver 
    dessertspoon 
    deunx 
    dextans 
    diamant 
    diamond 
    diamondtype 
    didotpoint 
    displacementton 
    dmtblack 
    dmtblue 
    dmtc 
    dmtcer 
    dmtceramic 
    dmtcoarse 
    dmte 
    dmtee 
    dmteefine 
    dmtefine 
    dmtf 
    dmtfine 
    dmtgreen 
    dmtred 
    dmtsilver 
    dmttan 
    dmtwhite 
    dmtx 
    dmtxcoarse 
    dmtxx 
    dmtxxcoarse 
    dodrans 
    doppelzentner 
    doricfoot 
    doubleremen 
    dr 
    draconicmonth 
    draconiticmonth 
    dram 
    droit 
    drop 
    drybarrel 
    drygallon 
    drypint 
    dryquart 
    dsp 
    dwt 
    earlyromanfoot 
    earthday 
    earthmass 
    earthmoonmass 
    earthradius 
    earthyear 
    eggyolkvolume 
    egyptiandigit 
    egyptianpalm 
    egyptianroyalcubit 
    egyptianshortcubit 
    eighthnote 
    emerald 
    engineerschain 
    engineerslink 
    english 
    englishcarat 
    equivalentlux 
    etti 
    etto 
    europeanpoint 
    europeshoesize 
    eushot 
    excelsior 
    extrafineindia 
    facecord 
    fathom 
    feet 
    fifth 
    finecrystolon 
    fineindia 
    fineounce 
    finger 
    fingerbreadth 
    fingerlength 
    firkin 
    firlot 
    flag 
    foot 
    force 
    fortnight 
    fothers 
    fotmal 
    fournierpoint 
    freightton 
    frenchcathetersize 
    frenchgrain 
    frenchpound 
    ft 
    furlong 
    g 
    gallon 
    galvat 
    gamma 
    garamond 
    geometricpace 
    germandidotpoint 
    gig 
    gill 
    gm 
    gr 
    grain 
    gram 
    gramme 
    gravity 
    greatprimer 
    greekcubit 
    greekfathom 
    greekfeet 
    greekfinger 
    greekfoot 
    greekkotyle 
    gregorianyear 
    grobe_kanon 
    grobe_sabon 
    gunterschain 
    gurleychain 
    gurleylink 
    halfnote 
    hand 
    hardblackarkansas 
    hardtranslucentarkansas 
    hardwhitearkansas 
    hashimicubit 
    heapedbushel 
    hebrewcubit 
    hefnercandle 
    hefnerunit 
    hekteos 
    hemidemisemiquaver 
    hemina 
    heminae 
    heredia 
    heredium 
    hide 
    hogshead 
    homestead 
    hoppusboardfoot 
    hoppusfoot 
    hoppuston 
    hour 
    housecord 
    hr 
    hundredweight 
    imperialbarrel 
    imperialbeerbutt 
    imperialbeerhogshead 
    imperialbushel 
    imperialchaldron 
    imperialfirkin 
    imperialgallon 
    imperialheapedbushel 
    imperialhogshead 
    imperialquarter 
    imperialwinehogshead 
    in 
    inch 
    INFORMATION 
    INpoint 
    int 
    intacre 
    intamp 
    intampere 
    internationalyard 
    invcm 
    ionicfoot 
    islamicleapyear 
    islamicmonth 
    islamicyear 
    iudiptheria 
    iugera 
    iugerum 
    iuinsulin 
    iupenicillin 
    japancup 
    jeroboam 
    jewelerspoint 
    jiffies 
    jiffy 
    jigger 
    jugera 
    jugerum 
    julianyear 
    jupiterday 
    jupitermass 
    jupiterradius 
    jupiteryear 
    K 
    kanon 
    kayser 
    kelvin 
    key 
    kg 
    khous 
    kilogram 
    kleine_kanon 
    kleine_sabon 
    klick 
    knot 
    kolonel 
    korpus 
    kus 
    L 
    lambda 
    landarea 
    last 
    lateromanfoot 
    lb 
    lbm 
    leadstone 
    leadwey 
    league 
    leapyear 
    legalcup 
    legaltablespoon 
    legaltbsp 
    LENGTH 
    leo 
    li 
    liang 
    libra 
    librae 
    lid 
    lightminute 
    lightsecond 
    line 
    link 
    liquidbarrel 
    liter 
    litre 
    longhundredweight 
    longprimer 
    longton 
    LUMINOUS_INTENSITY 
    l_P 
    m 
    mach 
    magnum 
    mancus 
    marsday 
    marsmass 
    marsradius 
    marsyear 
    mas 
    MASS 
    mast 
    maund 
    Mcf 
    mcg 
    mcm 
    medimnos 
    mediumcrystalon 
    mediumindia 
    meg 
    mendenhallyard 
    mercounce 
    mercpennyweight 
    mercpound 
    mercuryday 
    mercurymass 
    mercuryradius 
    mercuryyear 
    meridian 
    meter 
    methuselah 
    metre 
    metretes 
    metriccarat 
    metriccup 
    metricfifth 
    metricgrain 
    metricounce 
    metricpoint 
    metricquart 
    metrictenth 
    metricton 
    mi 
    micron 
    mignonette 
    mil 
    milangle 
    mile 
    millionth 
    min 
    minim 
    minimnote 
    minion 
    minute 
    missal 
    mite 
    mittel 
    mo 
    modii 
    modius 
    mol 
    mole 
    MONEY 
    month 
    moongravity 
    moonlum 
    moonmass 
    moonradius 
    mounce 
    MUSICAL_NOTE_LENGTH 
    m_P 
    navycablelength 
    nebuchadnezzar 
    neptuneday 
    neptunemass 
    neptuneradius 
    neptuneyear 
    newhayload 
    newhaytruss 
    nit 
    nodicalmonth 
    nonpareil 
    nonpareille 
    nonplusultra 
    nook 
    northerncubit 
    northernfoot 
    number10can 
    number1can 
    number2can 
    oatbushel 
    oceanarea 
    octant 
    oldhayload 
    oldhaytruss 
    oldjupitermass 
    oldliter 
    oldmarsmass 
    oldmercurymass 
    oldneptunemass 
    oldplutomass 
    oldpoint 
    oldsaturnmass 
    olduranusmass 
    oldvenusmass 
    ollock 
    olympicamma 
    olympicbema 
    olympiccord 
    olympiccubit 
    olympicdakylos 
    olympicfathom 
    olympicfeet 
    olympicfinger 
    olympicfoot 
    olympicorguia 
    olympicpace 
    olympicpalestra 
    olympicpalm 
    olympicplethron 
    olympicspan 
    olympicspithame 
    olympicstadion 
    orguia 
    ounce 
    oz 
    ozt 
    pace 
    pakistanmaund 
    pakistanseer 
    palmlength 
    paperM 
    paragon 
    parasang 
    passus 
    pearl 
    pedes 
    pennyweight 
    perch 
    periot 
    perl 
    persianroyalcubit 
    pes 
    pesdrusianus 
    petit 
    petroleumbarrel 
    pfund 
    pica 
    picul 
    pin 
    pinlength 
    plancklength 
    planckmass 
    plancktime 
    platinumounce 
    plethron 
    plutoday 
    plutomass 
    plutoradius 
    plutoyear 
    podes 
    point 
    pointangle 
    pointthickness 
    ponykeg 
    ponyvolume 
    postscriptpoint 
    pottle 
    pound 
    pous 
    printerspoint 
    pspoint 
    pt 
    puncheon 
    Q 
    quadrans 
    quadrant 
    quadrantal 
    quartaria 
    quartarius 
    quarter 
    quarternote 
    quaver 
    quincunx 
    quintal 
    quintant 
    radian 
    ramsdenschain 
    ramsdenslink 
    rd 
    registerton 
    rehoboam 
    remendigit 
    retmaunit 
    rev 
    revolution 
    ricebushel 
    rod 
    rollwallpaper 
    romanaspound 
    romancubit 
    romandigit 
    romanfeet 
    romanfoot 
    romaninch 
    romanobol 
    romanounce 
    romanpace 
    romanpalm 
    romanperch 
    romanpound 
    RU 
    ruby 
    russianmile 
    ryebushel 
    s 
    sabin 
    sailmakersyard 
    salmanazar 
    saturnday 
    saturnmass 
    saturnradius 
    saturnyear 
    sazhen 
    scriptulum 
    scrupula 
    seam 
    seamile 
    sec 
    second 
    section 
    seer 
    semibreve 
    semidemisemiquaver 
    semiquaver 
    semis 
    semisextula 
    semodii 
    semodius 
    semuncia 
    sennight 
    seprunx 
    ser 
    sescuncia 
    sextans 
    sextant 
    sextarii 
    sextarius 
    sextula 
    shaftment 
    shippington 
    shoeiron 
    shoeounce 
    shoesize_delta 
    shoe_boys0 
    shoe_girls0 
    shoe_men0 
    shoe_women0 
    shortquarter 
    shortquarterweight 
    shortton 
    shot 
    siderealday 
    siderealhour 
    siderealminute 
    siderealmonth 
    siderealsecond 
    siderealyear 
    sign 
    silverdirhem 
    silverkirat 
    silverrotl 
    silversmithpoint 
    silverwukiyeh 
    siscilius 
    sixteenthnote 
    sixtyfourthnote 
    sizeAring 
    sizeBring 
    sizeCring 
    sizeDring 
    sizeEring 
    sizeFring 
    sizeGring 
    sizeHring 
    sizeIring 
    sizeJring 
    sizeKring 
    sizeLring 
    sizeMring 
    sizeNring 
    sizeOring 
    sizePring 
    sizeQring 
    sizeRring 
    sizeSring 
    sizeTring 
    sizeUring 
    sizeVring 
    sizeWring 
    sizeXring 
    sizeYring 
    sizeZring 
    skeincotton 
    skylum 
    skylum_o 
    smallpica 
    smi 
    softarkansas 
    solarmass 
    solaryear 
    SOLID_ANGLE 
    soybeanbushel 
    sphere 
    sphericalrightangle 
    split 
    squareactus 
    squarearcmin 
    squarearcsec 
    squaredegree 
    squareminute 
    squaresecond 
    sr 
    stack 
    stadion 
    standardtemp 
    statutemile 
    stdatmT0 
    stdtemp 
    steradian 
    stickbutter 
    stone 
    strike 
    sumeriancubit 
    sumerianfoot 
    sunlum 
    sunlum_h 
    sunmass 
    surveychain 
    surveyorschain 
    surveyorslink 
    surveyorspole 
    susi 
    svedberg 
    sverdrup 
    t 
    tablespoon 
    taijin 
    teaspoon 
    TEMPERATURE 
    TEMPERATURE_DIFFERENCE 
    tempK 
    tempR 
    temprankine 
    tenth 
    tertia 
    texpoint 
    texscaledpoint 
    texsp 
    text 
    thermalvolt 
    thirtysecondnote 
    thou 
    Tim 
    timberfoot 
    TIME 
    timeatom 
    timeminute 
    timeostent 
    timeounce 
    timepoint 
    Tm 
    tod 
    tola 
    ton 
    tonne 
    towergrain 
    towerounce 
    towerpennyweight 
    towerpound 
    township 
    tradedirhem 
    tradekirat 
    traderotl 
    tradewukiyeh 
    triens 
    tronpound 
    tronstone 
    tropicalyear 
    troughtonyard 
    troyounce 
    troypound 
    turn 
    twip 
    t_P 
    u 
    uncia 
    unciae 
    uranusday 
    uranusmass 
    uranusradius 
    uranusyear 
    US 
    USacre 
    usbushel 
    UScable 
    uscup 
    USdimeweight 
    USdoubletimepace 
    usfirkin 
    usfloz 
    usfluidounce 
    usgallon 
    usgill 
    UShalfdollarweight 
    ushogshead 
    USmilitarypace 
    uspeck 
    uspint 
    usquart 
    USquarterweight 
    ustablespoon 
    ustbl 
    ustblsp 
    ustbsp 
    usteaspoon 
    ustsp 
    venusday 
    venusmass 
    venusradius 
    venusyear 
    verst 
    versta 
    violle 
    virgate 
    VOLUME 
    washita 
    waterton 
    wavenumber 
    week 
    wheatbushel 
    wholenote 
    wiendisplacement 
    winebottle 
    wineglass 
    winekeg 
    winesplit 
    wingchain 
    winglink 
    wk 
    woolbolt 
    woolclove 
    woollast 
    woolsack 
    woolsarpler 
    woolstone 
    wooltod 
    woolwey 
    xestes 
    XPT 
    yard 
    yd 
    year 
    yr 
    zentner 
    ° 
    °C 
    °F 
    °K 
    °R 
    ʒ 
    ℃ 
    ℉ 
    ℓ 
    ℔ 
    ℥ 
    K 
    ㍶ 
    ㎀ 
    ㎁ 
    ㎂ 
    ㎃ 
    ㎄ 
    ㎅ 
    ㎆ 
    ㎇ 
    ㎍ 
    ㎎ 
    ㎏ 
    ㎕ 
    ㎖ 
    ㎗ 
    ㎘ 
    ㎙ 
    ㎚ 
    ㎜ 
    ㎝ 
    ㎞ 
    ㎟ 
    ㎠ 
    ㎢ 
    ㎣ 
    ㎤ 
    ㎦ 
    ㎧ 
    ㎨ 
    ㎰ 
    ㎱ 
    ㎲ 
    ㎳ 
    ㏄ 
    ㏅ 
    ㏈ 
    ㏌ 
    ㏏ 
    ㏕ 
    ㏖ 
    ㏛ 
    台斤
    
