Red [
   description: { universal units converter based on units Unix utility }
   author: { Maxim Velesyuk }
   version: 1.0
]

but-last: func [str /size n] [
   unless n [n: 1]
   copy/part str ((index? tail str) - n - 1)
]

starts-with: func [series item /local tmp] [
   tmp: find/case series item
   all [tmp 1 = index? tmp]
]

unit: object [
   type: 'unit
   conversions: none
   base-unit: none
   prefix: 1
   init: does [
      conversions: make map! []
   ]
   base-unit-value: does [
      if base-unit [ convert 1 base-unit ]
   ]
   convert: func [x to_ /local tmp] [
      if tmp: select/case conversions to_ [
         x * prefix * tmp
      ]
   ]
]

units: context [
   graph: make map! []
   prefixes: make hash! []
   constants: make hash! []
   add-constant: func [name value] [ append append constants to-word name value ]
   add-unit: func [name /alias al /basic /local u] [
      name: to-word name

      if alias [
         put/case graph name u: find-unit to-word al
         return u
      ]
      u: make unit []
      u/init
      if basic  [
         u/type: 'basic
         put/case u/conversions name 1
         u/base-unit: name
      ]
      put/case graph name u
      u
   ]
   find-unit: func [name /local res p s-name match] [
      res: select/case graph name: to-word name
      s-name: to-string name
      if all [not res #"s" = last s-name] [
         name: but-last s-name         
         res: select/case graph name
      ]
      if all [not res (length? s-name) > 1] [
         match: ""
         foreach p prefixes [
            if starts-with s-name p [
               if (length? to-string p) > (length? match) [ match: to-string p ]
            ]
         ]
         if not empty? match [
            name: to-word next at s-name length? match
            res: select/case graph name
            res: copy res
            res/prefix: select constants to-word match
         ]
      ]
      res
   ]

   item: spec: none

   parse-formula: func [
      formula
      /debug
      /local res nums chars
      main-rule float-rule number-rule names tmp tmp2 units
      negate prev-value
   ] [
      names: copy []
      nums: charset [#"0" - #"9" #"."]
      chars: charset [#"a" - #"z" #"A" - #"Z"]
      float-rule: [ some nums opt ["e" opt "-" some nums] ]
      number-rule: [ float-rule | some nums ]
      units: copy []
      res: parse formula main-rule: [
         collect any [
            (negate: false)
            copy tmp [opt "-" number-rule]
            opt [if (prev-value) keep ('*)]
            keep (to-float tmp)
            (prev-value: true)
            | opt ["-" (negate: true)]
              copy tmp [chars any [chars | nums]] (append names tmp: to-word tmp)
              opt [if (prev-value) keep ('*)]
              opt [if (negate) keep (-1) keep ('*)]
              keep ( any [
                    select/case constants tmp
                    all [ tmp2: find-unit tmp
                          tmp2/base-unit-value
                          append units tmp2/base-unit
                          tmp2/base-unit-value ]
                    tmp
              ] )
              (prev-value: true)
            | "("
               opt [if (prev-value) keep ('*)]
               main-rule ")"
               (prev-value: true)
            | ahead ")" break
            | " "
            | "|" keep ('/) (prev-value: false)
            | "/" keep ('/) (prev-value: false)
            | "-" keep ('-) (prev-value: false)
            | "+" keep ('+) (prev-value: false)
            | "*" keep ('*) (prev-value: false)
            | "^^" keep ('**) (prev-value: false)
            | skip
         ]
      ]
      if debug [ probe res ]
      if res [
         either 1 = length? res [
            if word? res/1 [ units: reduce [res/1] res: copy [] ]
         ] [
            either word? res/1 [
               units: res
               res: copy []
            ] [
               tmp: none
               res: next res
               forall res [
                  if all [ '* = res/-1 word? res/1 ] [
                     tmp: (index? res) - 1
                  ]
               ]
               res: head res
               if tmp [
                  units: copy at res (tmp + 1)
                  res: copy/part res (tmp - 1)
               ]
            ]
         ]
      ]
      forall res [
         if block? res/1 [ res/1: to-paren res/1 ]
      ]
      if debug [ probe res ]
      res: reduce res
      reduce [res units]
   ]

   handle-formula: func [name formula /local spec units value u res f debug] [
      if attempt [ to-float name true ] [ throw 'bad-name ]
      name: to-word name

      debug: off

      formula: either debug [parse-formula/debug formula] [parse-formula formula]
      
      if debug [
         print [name mold formula]
      ]
      spec: formula/1
      units: formula/2
      if (length? units) > 1 [ throw 'unsupported-formula ]

      ; return true
      if all [ (length? spec) = 1 #"-" = last u: to-string name ] [
         name: to-word but-last u
         append prefixes name
         add-constant name spec/1
         return true
      ]

      ; just an alias
      if empty? spec [
         add-unit/alias name units/1
         return true
      ]
      ; simple constant
      if empty? units [
         add-constant name spec/1
         return true
      ]

      ; simple transformation
      if 1 = length? spec [
         value: find-unit units/1
         if debug [ probe value ]
         if value [
            u: add-unit name
            put/case u/conversions units/1 spec/1
            put/case value/conversions name (1 / spec/1)
            if value/type = 'basic [
               u/base-unit: units/1
            ]
         ]
      ]

      if 1 < length? spec [
         print ["huge spec:" mold spec]
         throw 'unsupported-spec
      ]
   ]
   parse-rule: [
      copy item any [not space skip] some space [
         "!" ( add-unit/basic to-word item )
         | copy spec collect [
            keep any [not [end | "#"] skip]
         ]
         ; copy spec [any not #"#" skip]
         (handle-formula item trim spec)
      ]
   ]
   
   file: %/usr/share/units/definitions.units
   
   load-from-scratch: func [/local lines line n err buff] [
      { loads initial definitons form a units file }
      clear graph
      clear constants
      clear prefixes
      ; add pi constant
      add-constant 'pi pi

      lines: read/lines file
      stats: #(total: 0 failed: 0 success: 0 errors: 0)
      n: 0
      buff: copy ""
      foreach line lines [
         line: trim line
         if #"#" = first line [ continue ]
         if #"!" = first line [ continue ]
         if 0 = length? line [ continue ]
         if #"\" = last line [
            append buff line
            continue
         ]
         if not empty? buff [
            line: copy append buff line
            clear buff
         ]
         n: n + 1
         if n = 50 [
            ; here comes dirty hacks
            ; add missing 'cm' manually
            parse "cm 0.01 m" parse-rule
         ]

         stats/total: stats/total + 1
         if err: try [ catch [
            ; parse line parse-rule
            parse line parse-rule
            stats/success: stats/success + 1
            false
         ] ] [
            ; print ["<<" err ":" line ":" n ]
            ; if error? err [ print err print line stats/errors: stats/errors + 1]
            stats/failed: stats/failed + 1
         ]
      ]
      stats/units: length? keys-of graph
      stats/constants: length? constants
      stats/prefixes: length? prefixes
      to-block stats
   ]
   
   convert: func [n from to_ /local from-unit to-unit] [
      { converts amount between specified units }
      unless all [
         from-unit: find-unit from
         to-unit: find-unit to_
      ] [ throw 'unit-not-supported ]

      unless all [ from-unit/base-unit = to-unit/base-unit ] [
         throw 'cant-convert
      ]

      unless number? n [
         n: select constants n
         unless n [ throw 'unknown-amount ]
      ]

      (n * from-unit/base-unit-value) / to-unit/base-unit-value
   ]
   
   dump: func [/local res units k v tmp cs] [
      { dumps units content into a loadable structure }
      units: copy []
      foreach [k v] to-block graph [
         append units k
         tmp: object [
            type: v/type
            base-unit: v/base-unit
            prefix: v/prefix
            conversions: v/conversions
         ]
         tmp: to-block tmp
         new-line/skip tmp on 2
         append/only units tmp
      ]
      new-line/skip units on 2
      cs: to-block constants
      new-line/skip cs on 2
      mold new-line/skip compose/deep [
         units: [(units)]
         constants: [(cs)]
         prefixes: [(to-block prefixes)]
      ] on 2
   ]

   load: func [src /local units cs pref tmp k v] [
      units: select src 'units
      cs: select src 'constants
      pref: select src 'prefixes
      clear graph

      clear prefixes
      foreach [k v] units [
         tmp: make unit v
         put graph k tmp
      ]
      constants: to-hash cs
      prefixes: to-hash pref
      compose [
         units: (length? keys-of graph)
         constants: (length? constants)
         prefixes: (length? prefixes)
      ]
   ]
]
